package com.icountries.googleapicrawler;

/**
 * Created by jorge on 27/02/17.
 */
public class RecommendedModel {

    private int id;
    private String name;

    public RecommendedModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

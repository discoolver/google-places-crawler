package com.icountries.googleapicrawler;

import org.apache.logging.log4j.Logger;
import se.walkercrou.places.GooglePlaces;
import se.walkercrou.places.Place;

import java.util.List;
import java.util.Queue;
import java.util.logging.LogManager;

/**
 * Created by jorge on 23/04/17.
 */
public class RecommendedGoogleQuerierRunnable implements Runnable {

    private RecommendedModel recommendedToFetch;
    //private GooglePlaces client = new GooglePlaces("AIzaSyCj7AybC5AkHm_YDOrriH0Tn5AVwBxOAkA");
    private GooglePlaces client = new GooglePlaces("AIzaSyBqp5zQ398s90xfei6oS5qvF8f72fa94TA");

    public RecommendedGoogleQuerierRunnable(RecommendedModel recommended) {
        this.recommendedToFetch = recommended;
    }

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(RecommendedGoogleQuerierRunnable.class);

    public void run() {
        logger.debug("Launched new consumer thread recommendedGoogleQuerier " + Thread.currentThread().getId());

        requestInformation(recommendedToFetch);


        logger.debug("{} Dying", Thread.currentThread().getId());
    }

    private void requestInformation(RecommendedModel modelToQuery) {
        Place place = searchForRecommendedInGoogle(modelToQuery);
        place = client.getPlaceById(place.getPlaceId());
        if (place != null) {
            Main.logInformation(modelToQuery, place);
        } else {
            logger.info(String.format("INFO NOT FOUND FOR RECOMMENDED {}, {}", modelToQuery.getId(), modelToQuery.getName()));
        }
    }

    private Place searchForRecommendedInGoogle(RecommendedModel modelToQuery){
        List<Place> places = client.getPlacesByQuery(modelToQuery.getName(), GooglePlaces.MAXIMUM_RESULTS);
        if (places.size() > 0) {
            return places.get(0);
        } else {
            return null;
        }
    }
}

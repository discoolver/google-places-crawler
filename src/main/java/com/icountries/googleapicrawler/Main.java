package com.icountries.googleapicrawler;

import com.google.gson.Gson;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.logging.log4j.core.util.ExecutorServices;
import se.walkercrou.places.Photo;
import se.walkercrou.places.Place;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;

/**
 * Created by jorge on 27/02/17.
 */
public class Main {

    private static final ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
    private static PrintWriter fileWriter;

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main (String[] args) throws Exception {
        logger.debug("STARTING JOB");
        //Thread databaseThread = new Thread(new DatabaseAccessor());
        //databaseThread.start();

        DatabaseAccessor accessor = new DatabaseAccessor();
        accessor.run();

        fileWriter = new PrintWriter(new FileOutputStream(new File("Recommended_"+System.currentTimeMillis())));



        boolean finished = false;
        while (!finished) {
            logger.debug("Waiting still");
            finished = executor.awaitTermination(1, TimeUnit.MINUTES);
        }
        logger.debug("Process finished");
    }

    public static void addJob(RecommendedModel rec){

        executor.execute(new RecommendedGoogleQuerierRunnable(rec));
    }





    public static synchronized void logInformation(RecommendedModel model, Place googlePlace) {


        List<String> photoUrls = googlePlace.getPhotos().stream()
                .map((Photo photo) -> photo.getReference())
                .collect(Collectors.toList());


        //googlePlace.getPhotos().stream().forEach(Main::writePhoto);
        Gson gson = new Gson();
        String toLog = String.format("%s;%s;%s;%s;%s;%s;%s", model.getId(), model.getName(), googlePlace.getName(),
                googlePlace.getAddress(), googlePlace.getHours(), googlePlace.getGoogleUrl(), gson.toJson(photoUrls));
        logger.warn(toLog);
        fileWriter.println(toLog);

    }


    public static void writePhoto (Photo photo) {
        BufferedImage image = photo.getImage();
        File file = new File(photo.getReference()+".jpg");
        try {
            ImageIO.write(image,"jpg", file);
        } catch (IOException ex){
            logger.error(ex.getMessage());
        }
    }






    private static class DatabaseAccessor implements Runnable {

        private static final int PAGE_SIZE = 10;

        private static final String QUERY = "SELECT ID_RECOMMENDED_BUSINESS, NAME FROM ic_recommended_business_data " +
                "WHERE ID_LANGUAGE = 1 ORDER BY ID_RECOMMENDED_BUSINESS DESC LIMIT "+PAGE_SIZE+" OFFSET ?";


        public void run() {
            try {
                startQueuingJobs();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void startQueuingJobs () throws Exception {

            logger.debug("REQUESTING DATABASE RECOMMENDED");

            TimeZone timeZone = TimeZone.getTimeZone("UTC"); // e.g. "Europe/Rome"
            TimeZone.setDefault(timeZone);

            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setUser("root");
            dataSource.setPassword("alonso");
            dataSource.setServerName("localhost");

            //dataSource.setUser("i_spain");
            //dataSource.setPassword("Gandalf31171seyspyuab");
            //dataSource.setServerName("82.223.81.247");

            dataSource.setDatabaseName("i-countries");
            dataSource.setPort(3306);

            int counter = 0;
            Connection conn = dataSource.getConnection();

            logger.debug("Database connection ACQUIRED");
            PreparedStatement stmt;
            ResultSet rs;
            boolean stop = false;
            while(!stop) {

                stmt = conn.prepareStatement(QUERY);
                stmt.setInt(1, counter * PAGE_SIZE);
                counter++;
                logger.debug("SQL: Requesting page {}", counter);
                logger.debug("Query: "+stmt.toString());
                rs = stmt.executeQuery();

                boolean areThereMoreRows = rs.next();
                if (!areThereMoreRows) {
                    logger.info("All values on database already checked from database.");
                    stop = true;
                } else {
                    while (areThereMoreRows) {
                        Main.addJob(new RecommendedModel(rs.getInt(1), rs.getString(2)));
                        areThereMoreRows = rs.next();
                    }

                    //ONLY REQUEST first page NOT TO MAX OUT THE REQUESTS WE HAVE
                    stop = true;
                }

                rs.close();
                stmt.close();


            }

            conn.close();
            logger.debug("Connection closed. Queuing thread dying");
        }

    }
}
